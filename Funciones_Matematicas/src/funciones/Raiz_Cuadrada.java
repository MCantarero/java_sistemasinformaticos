package funciones;
import java.lang.Math;
import java.util.Scanner;
public class Raiz_Cuadrada {

	public static void main(String[] args) {


		//Seccion de variables
		
		double a,b,c;
		String respuesta;
		boolean si=false;
	
		
		//Defino asi las letras ya que en un futuro espero poder acceder a ellas mediante un for
		//y guardar los datos dinamicamente
		char 	letras[];
		letras= new char[3];
				letras[0]= 'A';
				letras[1]= 'B';
				letras[2]= 'C';
		
		//objeto scaner para leer del teclado
		Scanner teclado= new Scanner(System.in);		
		do {
			System.out.println("Se va a Resolver una equacion de 2º Grado a su elección");
			
			System.out.print("Introduzca valor de " + letras[0] + ": ");
			a=teclado.nextDouble();
			System.out.print("\n");
			
			System.out.print("Introduzca valor de " + letras[1] + ": ");
			b=teclado.nextDouble();
			System.out.print("\n");
			
			System.out.print("Introduzca valor de " + letras[2] + ": ");
			c=teclado.nextDouble();
			System.out.print("\n");
			System.out.print("Los valores son: \n");
			System.out.print(a + " " + b + " " + c );
			System.out.print("\n");
			System.out.println("¿Los valores son correctos? Si/no");
			respuesta=teclado.next();
			
			if(respuesta.equals("Si")) {
				double f_parte_raiz=(Math.pow(b, 2)-4*a*c)/2*a;
				double resultado_positivo=-b+Math.sqrt(f_parte_raiz);
				double resultado_negativo=-b-Math.sqrt(f_parte_raiz);
				si=true;
				
				if(f_parte_raiz<0) {
					System.out.println("No tiene solucion en Reales ");
				
				}else {
					System.out.println("X_1 es: "+resultado_positivo);
					System.out.println("X_2 es: "+resultado_negativo);
				}
					
				
			}
			
		}	

		while(si==false); 

		

		teclado.close();
	}

}

