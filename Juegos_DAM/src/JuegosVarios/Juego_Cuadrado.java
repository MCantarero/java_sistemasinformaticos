package JuegosVarios;
import java.util.Scanner;

public class Juego_Cuadrado {
	
	public static void cuadrado() {
		
		Scanner teclado = new Scanner(System.in);
	
		int lado;
			
		System.out.println("Elegiste el juego del Cuadrado");
		
		System.out.println("Por favor indique el tamaño del lado de su cuadrado");
	
		lado=teclado.nextInt();
		
		for(int i=0; i<lado; i++ ) {//calcula el tamañano del cuadrado, he imprime los asteriscos y los espacios correspondientes
			for(int j=0; j<lado; j++) {
				if(i==0 || i==lado-1 || j==0 || j==lado-1 ) {
					System.out.print("* ");
				}else {System.out.print("  "); }
			}
		System.out.println();
		}
		//teclado.close();
	}

}
