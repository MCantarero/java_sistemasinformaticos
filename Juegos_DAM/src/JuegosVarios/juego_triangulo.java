package JuegosVarios;
import java.util.Scanner;

public class juego_triangulo {
	
	public static void triangulo() {
		
	  Scanner teclado = new Scanner(System.in);
		
	  int base;
			
		System.out.println("Elegiste el juego del Triangulo");
		System.out.println("Por favor indique el tamaño de la base de su Triangulo");
		base=teclado.nextInt();
		
		for(int i=0; i<base; i++) {//Estos bucles generan un triangulo hueco
			for(int j=0; j<base; j++) {
				if( i==base-1 || j==0 ) {System.out.print("*"); }else if(i==j) {System.out.print("*");}else {System.out.print(" ");}
				
			}
			System.out.println();
		}
		//teclado.close();
	}

}
